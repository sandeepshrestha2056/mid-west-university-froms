import React from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import './App.css';
import LibraryCard from './Forms/Library card/LibraryCard';
import ResultAnalysis from './Forms/Result Analysis/ResultAnalysis';
import InternalAssesssmentMark from './Forms/Internal Assessment mark slip/InternalAssesssmentMark';
import Pichart from './Forms/PiChart/Pichart';
import DeterminationLetterGrading from './Forms/Determination of Letter grading/DeterminationLetterGrading';
import ShortTermObjList from './Forms/Short term object list/ShortTermObjList';
import SalaryDetails from './Forms/Salary Details/SalaryDetails';
import AttendenceSheet from './Forms/Attendence Sheet/AttendenceSheet';
import ChalaniBook from './Forms/Chalani book/ChalaniBook';
import Registration from './Forms/Registration/Registration';
import AdmissionCardForm from './Forms/Admission/AdmissionCardForm';
import SubjectTriplicate from './Forms/EMO Subject Triplicate/SubjectTriplicate';
import DartaBook from './Forms/Darta Book/DartaBook';
import Navbar from './Navbar/Navbar';
import BillPage29 from './Forms/Bill page 29/BillPage29';
import BarChart from './Forms/Bar Chart/BarChart';
import TeacherPost from './Forms/Teacher Post/TeacherPost';
import PageNo33 from './Forms/Picture no33/PageNo33';
import Routine from './Forms/Routine/Routine';
import StudentAttendance from './Forms/Student Attendance Sheet/StudentAttendance';
import RoutineFirstThirdSem from './Forms/Routine First/RoutineFirstThirdSem';


function App() {
  return (
    <Router >
      <Navbar />
      <Routes>
        <Route path='/' element={<AdmissionCardForm />} />
        <Route path='/subjecttriplicate' element={<SubjectTriplicate />} />
        <Route path='/registration' element={<Registration />} />
        <Route path='/dartaBook' element={<DartaBook />} />
        <Route path='/chalaniBook' element={<ChalaniBook />} />
        <Route path='/attendenceSheet' element={<AttendenceSheet />} />
        <Route path='/salarydetails' element={<SalaryDetails />} />
        <Route path='/shorttermbbjList' element={<ShortTermObjList />} />
        <Route path='/determinationlettergrading' element={<DeterminationLetterGrading />} />
        <Route path='/pichart' element={<Pichart />} />
        <Route path='/internalassesssmentmark' element={<InternalAssesssmentMark />} />
        <Route path='/resultanalysis' element={<ResultAnalysis />} />
        <Route path='/libraryCard' element={<LibraryCard />} />
        <Route path='/billpage29' element={<BillPage29 />} />
        <Route path='/barchart' element={<BarChart />} />
        <Route path='/teacherpost' element={<TeacherPost />} />
        <Route path='/pictureno33' element={<PageNo33 />} />
        <Route path='/studentattendance' element={<StudentAttendance />} />
        <Route path='/routine' element={<Routine />} />
        <Route path='/routineoffirstndthird' element={<RoutineFirstThirdSem />} />
      </Routes>
    </Router>

  );
}

export default App;
