import styled from 'styled-components';

export const MainContainer = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
  p{
        color: rgb(91, 183, 219);
  }
`;

export const Container = styled.div`
display       : flex;
flex-direction: column;
overflow-x    : hidden;
width         : 55vw;
border        : 1px solid black;
margin        : 2rem;
`;
export const Heading = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  
  p:first-child{
        font-weight:bold;
       font-size: 2.5rem; 
       margin-bottom:0.4rem;
       
  }
  p:last-child{
        font-size: 1.2rem;
        margin:0.5rem 0;
  }
`;
export const Heading2 = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const Img = styled.div`
  img{
        max-width:10vmax;
  }
`;
export const Mid = styled.div`
  border:2px solid black;
  border-radius: 10px;
  box-shadow: 6px 5px 2px 1px rgb(53, 156, 197);
  border-color:rgb(91, 183, 219);
  display:flex;
  justify-content:center;
  align-items:center;
  height:5vmax;
  margin-top:2rem;
  padding:0 .5rem;
  p{
        text-decoration:underline;
        font-size:1.3rem;
  }
`;
export const Name = styled.p`
margin: 1.5rem 2rem 0.4rem 2rem;
font-size: 1.4rem
`;

export const InfoDiv = styled.div`
display: flex;
 justify-content: space-between;
 align-items:center;
  margin: 0 2rem;
  
  p{
        margin:0.5rem 0;
        font-size: 1.4rem
  }
`;