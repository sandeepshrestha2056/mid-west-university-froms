import React, { useRef } from 'react'
import ReactToPrint from "react-to-print";
import { Container, Heading, Heading2, Mid, Img, MainContainer, Name, InfoDiv } from './style'

const LibraryCard = () => {
      const currentRef = useRef(null);
      return (
            <MainContainer>
                  <Container ref={currentRef}>
                        <Heading>
                              <p>Mid-Western University</p>
                              <p>Birendranagar, Surkhet</p>
                        </Heading>
                        <Heading2>
                              <Img>
                                    <img src="https://png.pngtree.com/element_pic/00/16/07/115783931601b5c.jpg" alt="logo" />
                              </Img>
                              <Mid>
                                    <p>LIBRARY-CARD</p>
                              </Mid>
                              <Img>
                                    <img src="https://www.seekpng.com/png/full/847-8474751_download-empty-profile.png" alt="person" />
                              </Img>
                        </Heading2>
                        <div>
                              <Name>Name: Kashar Budha</Name>

                              <InfoDiv>
                                    <p>Year/Semester: I</p>
                                    <p>Roll No: 31078</p>
                              </InfoDiv>
                              <InfoDiv style={{ marginBottom: "0.7rem" }}>
                                    <p>Faculty: Education</p>
                                    <p>Level: </p>
                              </InfoDiv>
                        </div>
                  </Container>
                  <ReactToPrint
                        trigger={() => <button>Print</button>}
                        content={() => currentRef.current}
                  />
            </MainContainer>
      )
}

export default LibraryCard