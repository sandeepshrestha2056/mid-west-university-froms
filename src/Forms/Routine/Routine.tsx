import React from 'react'
import { BottomDiv, Container, Heading, MainContainer, StyledTable, TopBar } from './style'

const Routine = () => {
      const data = [
            {
                  sem: "1st",
                  t6: [
                        { subj1: 'COM.Eng-411(BDG)-(23)' }
                  ],
                  t7: [
                        { subj1: "COM.Nep.412(RPD)-(23)" }
                  ],
                  t8: [
                        {
                              subj1: "HEP.415(HPJ)-(20)",
                              subj2: "POP.415(KRS)-(5)",
                              subj3: "ENG.415(DBG)-(3)",
                              subj4: "NEP.415(AKC)-(9)",
                        }
                  ],
                  t9: [
                        {
                              subj1: "HEP.414(IPR)-(20)",
                              subj2: "POP.414(KRS)-(5)",
                              subj3: "ENG.415(DBG)-(3)",
                              subj4: "NEP.415(AKC)-(9)",
                        }
                  ],
                  t10: [
                        {
                              subj1: "HEP.414(IPR)-(20)",

                        }
                  ],
                  t11: [
                        {
                              subj1: "HEP.414(IPR)-(20)",

                        }
                  ],

            }
      ]
      const campusChiefName = "Birendra Raj Lamsal"
      return (
            <MainContainer>
                  <Container>
                        <TopBar>
                              <p>MID_WEST UNIVERSITY</p>
                              <p>BABAI MULTIPLE CAMPUS</p>
                              <p>GULARIYA BARDIYA</p>
                              <p>Routine: 2078(2022)</p>
                              <p>Level: Bachelor</p>
                        </TopBar>
                        <Heading>
                              <p>Faculty: Education</p>
                              <p>Semesters: 1,2,3,5</p>
                        </Heading>

                        <StyledTable >
                              <thead>
                                    <tr>
                                          <th>Semes</th>
                                          <th>6:30am-7:30am</th>
                                          <th>7:30am-8:30am</th>
                                          <th>8:30am-9:30am</th>
                                          <th>9:30am-10:30am</th>
                                          <th>10:30am-11:30am</th>
                                          <th>11:30am-12:30am</th>
                                    </tr>
                              </thead>
                              <tbody>
                                    {data.map((item, i) => (
                                          <tr>
                                                <td>{item.sem}</td>
                                                <td>{item.t6.map((i) => (<>
                                                      <p>{i.subj1}</p>
                                                </>))}</td>
                                                <td>{item.t7.map((i) => (
                                                      <>
                                                            <p>{i.subj1}</p>
                                                      </>
                                                ))}</td>
                                                <td>{item.t8.map((i) => (
                                                      <>
                                                            <p>{i.subj1}</p>
                                                            <p>{i.subj2}</p>
                                                            <p>{i.subj3}</p>
                                                            <p>{i.subj4}</p>
                                                      </>
                                                ))}</td>
                                                <td>{item.t9.map((i) => (
                                                      <>
                                                            <p>{i.subj1}</p>
                                                            <p>{i.subj2}</p>
                                                            <p>{i.subj3}</p>
                                                            <p>{i.subj4}</p>
                                                      </>
                                                ))}</td>
                                                <td>{item.t10.map((i) => (
                                                      <>
                                                            <p>{i.subj1}</p>
                                                      </>
                                                ))}</td>
                                                <td>{item?.t11.map((i) => (
                                                      <>
                                                            <p>{i.subj1}</p>
                                                      </>
                                                ))}</td>
                                          </tr>
                                    ))}
                              </tbody>
                        </StyledTable>
                        <BottomDiv>
                              <p>Head of Instruction Committee</p>
                              <div>
                                    <p>{campusChiefName}</p>
                                    <p>Campus Chief</p>
                              </div>
                        </BottomDiv>
                  </Container>
            </MainContainer>
      )
}

export default Routine