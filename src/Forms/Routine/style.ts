import styled from 'styled-components';

export const MainContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 70vw;
`;

export const TopBar = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
p{
  margin: 0.4rem 0;
}
  
`;

export const BottomDiv = styled.div`
  display: flex;
  justify-content:space-between;
  margin: 1.6rem 1rem ;
  p{
    text-align: center;
  }
`;

export const Heading = styled.div`
  display: flex;
  justify-content:space-between;
  margin: 0.2rem 1rem ;
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border:1px solid black;
    width:69vw;
    margin-left: .5rem;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      color:black;
      margin: 0;
      padding: 0.5rem;
      font-size:1.4rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }

    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      font-size:1.2rem;

      :last-child {
        border-right: 0;

      } 
    }
  
`;