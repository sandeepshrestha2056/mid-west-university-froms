import React from 'react'
import { Container, InfoDiv, MainContainer, P, StyledTable } from './style';

const PageNo33 = () => {
      const data = [
            { sn: 1, name: "name", designation: "designation", email: "myemail@getMaxListeners.com" },
            { sn: 1, name: "name", designation: "designation", email: "myemail@getMaxListeners.com" },
            { sn: 1, name: "name", designation: "designation", email: "myemail@getMaxListeners.com" },
            { sn: 1, name: "name", designation: "designation", email: "myemail@getMaxListeners.com" }
      ];
      const info = {
            name: "Pancha Dev Bhatta",
            post: [
                  {
                        name: "Assistant Professor",
                        nameoFInstitution: "Centeral Campus of Sc & Tech, MU"
                  },
                  {
                        name: "IT Export",
                        nameoFInstitution: "Secretatriat of the Vice-Chancellor, MU"
                  },

            ],
            phone: '9848842460',
            email: "bhattapd@mwu.edu.np",
            website: "techversary.com",
      }
      return (
            <MainContainer>
                  <Container>
                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th>SN</th>
                                          <th>Name</th>
                                          <th>Designation</th>
                                          <th>Email</th>
                                    </tr>
                              </thead>
                              <tbody>
                                    {data.map((item) => (
                                          <tr>
                                                <td style={{ backgroundColor: "rgb(32, 125, 161)" }}>{item.sn}</td>
                                                <td>{item.name}</td>
                                                <td>{item.designation}</td>
                                                <td>{item.email}</td>
                                          </tr>
                                    ))}
                              </tbody>
                        </StyledTable>
                        <P>With regards,</P>
                        <InfoDiv>
                              <p>{info.name}</p>
                              {info.post.map((item) => (
                                    <p><strong>{item.name} </strong>({item.nameoFInstitution})</p>
                              ))}
                              <p> <strong>Phone:(+977)</strong> {info.phone} | <strong>O email:</strong>{info.email}</p>
                              <p><strong>Website: </strong>{info.website}</p>
                              <p>Website | Email | Facebook | Twitter | Youtube</p>
                        </InfoDiv>
                  </Container>
            </MainContainer>
      )
}

export default PageNo33