import styled from 'styled-components';

export const MainContainer = styled.div`
  display: flex;
  justify-content: center;

`;
export const Container = styled.div`
  display:flex;
  width:50vw;
  flex-direction:column;
  border:1px solid black;
  margin-bottom:3rem;
`;

export const P = styled.p`
  margin-left:2rem;
`;

export const InfoDiv = styled.div`
  margin-left: 2rem;
  p{
        font-size:1.2rem;
  }
  p:first-child{
        font-size: 1.4rem;
        font-weight: bold;
      }
      color: rgb(27, 103, 133)
`;
export const StyledTable = styled.table`
    border-spacing: 0;
    border:1px solid black;
    width:50vw;
margin-left:-2px;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      background-color:rgb(32, 125, 161);
      color:black;
      margin: 0;
      padding: 0.5rem;
      font-size:1.4rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      font-size:1.2rem;

      :last-child {
        border-right: 0;

      } 
    }
`;