import styled from 'styled-components';

export const MainContainer = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
`;

export const Container = styled.div`
display       : flex;
flex-direction: column;
overflow-x    : hidden;
width         : 55vw;
border        : 1px solid black;
margin        : 2rem;
`;

export const Headers = styled.div`
display              : grid;
grid-template-columns: 1fr 2fr;
`;

export const Logo = styled.div`
margin: 1rem 1rem;
  img{
      max-width: 10vmax;
  }
`;

export const Mid = styled.div`
margin-top:2rem;
justify-self: self-start;
p{
      color: rgb(46, 120, 150);
}
`;

export const H1 = styled.p`
font-size : 3rem;
text-align: center;
margin : 0;
`;

export const H2 = styled.p`
font-size : 1.5rem;
text-align: center;
margin : 0;
`;

export const H3= styled.p`
padding-top: .9rem;
font-size  : 1.3rem;
text-align : center;
margin : 0;
`;

export const CardName = styled.h2`
text-align: center;
font-size : 2.5rem;
color     : rgb(204, 59, 59);
margin:0.4rem 0;
`;

export const Infos = styled.div`
padding-left: 1.5rem;
p{
      font-size: 1.2rem;
      span{
            color: rgb(46, 120, 150);
}
      }
`;

export const P = styled.p`
font-weight  : 500;
text-align   : right;
padding-right: 1rem;
color        : rgb(46, 120, 150);
white-space  : pre;
`;