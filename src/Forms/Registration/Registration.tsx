import React from 'react';
import { CardName, Container, H1, H2, H3, Headers, Infos, Logo, MainContainer, Mid, P } from './styled';

const Registration = () => {
      return (
            <MainContainer>
                  <Container>
                        <Headers>
                              <Logo>
                                    <img src="https://image.shutterstock.com/image-vector/earth-globe-icon-trendy-logo-260nw-1257541729.jpg" alt="" />
                              </Logo>
                              <Mid>
                                    <H1>Mid-Western University</H1>
                                    <H2>Examinations Management Office</H2>
                                    <H3>Surkhet, Nepal</H3>
                              </Mid>

                        </Headers>
                        <CardName>Registration Card</CardName>
                        <Infos>
                              <p className="reg"><span>Reg.No.: </span>2021-67-3-1010-0046</p>
                              <p className="name"><span>Student's Name:</span> Bishal Khadka</p>
                              <p className="institution"><span>Institution:</span> Global College International</p>
                              <p><span>Date of Birth:</span> 2056.03/29(13/07/1994 A.D)</p>
                              <p><span>Issue Date:</span> 04/01/2022</p>
                        </Infos>
                        <P>Checked by     <strong>Examinations Management Chief</strong></P>
                  </Container>
            </MainContainer>
      )
}

export default Registration