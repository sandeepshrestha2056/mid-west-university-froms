import React from 'react'
import { Bar } from "@iftek/react-chartjs-3";

const BarChart = () => {
      return (
            <div style={{ width: "40vw" }}>
                  <Bar
                        data={{
                              labels: ['BA-75', 'BA-76', 'BA-77'],
                              datasets: [
                                    {
                                          label: "boy",
                                          backgroundColor: ['#41B883', '#E46651', '#00D8FF'],
                                          data: [43, 34, 11]
                                    },
                                    {
                                          label: "girl",
                                          backgroundColor: ['#456673', '#C48651', '#00FF'],
                                          data: [53, 54, 201]
                                    },
                              ]
                        }}
                  />
            </div>
      )
}

export default BarChart