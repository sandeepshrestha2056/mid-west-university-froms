import React, { useRef } from 'react';
import { useReactToPrint } from "react-to-print"
import { Pie } from "@iftek/react-chartjs-3";

const Pichart = () => {
      const componentRef = useRef(null);
      const handlePrint = useReactToPrint({
            content: () => componentRef.current,
      });
      return (
            <div>


                  <div ref={componentRef} style={{ width: "40vw", border: "1px solid black", padding: "2rem" }}>
                        <Pie
                              width={100}
                              height={50}
                              data={{
                                    labels: ['VueJs', 'EmberJs', 'ReactJs', 'AngularJs'],
                                    datasets: [
                                          {
                                                backgroundColor: ['#41B883', '#E46651', '#00D8FF', '#DD1B16'],
                                                data: [40, 20, 80, 10],
                                          },]

                              }} />
                  </div>
                  <button onClick={handlePrint}>Print this out!</button>
            </div>
      )
}

export default Pichart