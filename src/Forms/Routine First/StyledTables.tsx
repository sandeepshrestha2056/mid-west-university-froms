import React from 'react'
import { StyledTable } from './style'

type Props = {
      data: Array<{
            lecturer: string,
            first: number,
            third: number,
            total(): number
      }>;
}

const StyledTables = ({ data }: Props) => {
      return (
            <StyledTable style={{ width: "22vw" }}>
                  <thead>
                        <tr>
                              <th rowSpan={2}>Lecturer</th>
                              <th colSpan={2}>Semester/Period</th>
                              <th rowSpan={2} >Total</th>
                        </tr>
                        <tr>
                              <th>First</th>
                              <th>Third</th>
                        </tr>

                  </thead>

                  <tbody>
                        {data.map((i) => (
                              <tr>
                                    <th>{i.lecturer}</th>
                                    <th>{i.first}</th>
                                    <th>{i.third}</th>
                                    <th>{i.total()}</th>
                              </tr>
                        ))}
                  </tbody>
            </StyledTable>
      )
}

export default StyledTables