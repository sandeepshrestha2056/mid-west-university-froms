import React from 'react';
import { BottomDiv, Header, Header2, P, StyledTable, TableDiv } from './style';
import StyledTables from './StyledTables';

const RoutineFirstThirdSem = () => {
      const data1 = [
            { lecturer: "BRL", first: 1, third: 1, total() { return this.first + this.third } },
            { lecturer: "RDP", first: 1, third: 3, total() { return this.first + this.third } },
            { lecturer: "MA", first: 1, third: 1, total() { return this.first + this.third } },
            { lecturer: "LG", first: 1, third: 1, total() { return this.first + this.third } },
            { lecturer: "HHA", first: 1, third: 1, total() { return this.first + this.third } },
      ]

      return (
            <div>
                  <div>
                        <Header>
                              <p>MID_WEST UNIVERSITY</p>
                              <p>BABAI MULTIPLE CAMPUS</p>
                              <p>GULARIYA , BARDIYA</p>
                              <p>ROUTINE 20.....</p>
                              <p>SEMESTER FIRST & THIRD</p>
                        </Header>

                        <Header2>
                              <div>
                                    <p>LEVEL: MASTER</p>
                                    <p>FACULTY: EDUCATION</p>
                              </div>
                              <div>
                                    <p>Shift: Evening(3:00 PM to 6:00 PM)</p>
                              </div>
                        </Header2>

                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th rowSpan={2} >MASTER</th>
                                          <th colSpan={6}>TIME</th>
                                    </tr>
                                    <tr>
                                          <th colSpan={2}>3:00-4:00</th>
                                          <th colSpan={2}>4:00-5:00</th>
                                          <th colSpan={2}>5:00-6:00</th>
                                    </tr>
                                    <tr>
                                          <th>Semester</th>
                                          <th>First</th>
                                          <th>Third</th>
                                          <th>First</th>
                                          <th>Third</th>
                                          <th>First</th>
                                          <th>Third</th>
                                    </tr>
                              </thead>
                        </StyledTable>
                        <P>Work Load of Lecturer</P>
                        <TableDiv>
                              <StyledTables data={data1} />
                              <StyledTables data={data1} />
                              <StyledTables data={data1} />
                              <StyledTables data={data1} />

                        </TableDiv>
                        <BottomDiv>
                              <div>
                                    <p>Name</p>
                                    <p>Post</p>
                              </div>
                              <div>
                                    <p>Name</p>
                                    <p>Post</p>
                              </div>
                        </BottomDiv>
                  </div>
            </div>
      )
}

export default RoutineFirstThirdSem;