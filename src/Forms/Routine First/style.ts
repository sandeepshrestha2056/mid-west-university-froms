import styled from 'styled-components';

export const Header = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
      p{
            margin: .5rem 0;
      }

`;

export const Header2 = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0 1rem;
  p{
        margin: 0.2rem;
  }
`;

export const P = styled.p`
  text-align: center;
  font-size: 1.3rem;
  font-weight: bold;
  font-weight: bold;
  margin: 2rem 0;
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border:1px solid black;
    width:98.5vw;
    margin-left: .5rem;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      color:black;
      margin: 0;
      padding: 0.5rem;
      font-size:1.1rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }

    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      font-size:1rem;

      :last-child {
        border-right: 0;

      } 
    }
  
`;

export const TableDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

`;

export const BottomDiv = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 2rem 4rem;
  div{
        p{
              text-align: center;
        }
  }
`;