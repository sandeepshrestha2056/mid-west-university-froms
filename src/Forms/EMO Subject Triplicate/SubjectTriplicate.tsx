import React from 'react'
import { BodyDiv, Div, H1, H2, H3, LeftDiv, StyledTable, TopHeading } from './Styles'


const SubjectTriplicate = () => {
  const data = [
    { sn: 1, year: 2021, program: "Bachelor of Arts", name: "ANITA SARU MAGAR", college: "1003", rollno: 2100000000, type: "Partial", reg: "2018-69-2-1003-0004", subj1: "", subj2: "", subj3: "", subj4: "", subj5: "", subj6: "", subj7: "", subj8: "", subj9: "", },
    { sn: 2, year: 2021, program: "Bachelor of Arts", name: "ANITA SARU ", college: "1003", rollno: 2100000020, type: "Partial", reg: "2018-69-3-1003-0004", subj1: "", subj2: "", subj3: "", subj4: "", subj5: "", subj6: "", subj7: "", subj8: "", subj9: "", }
  ]
  return (
    <Div>
      <TopHeading >
        <H1>Mid-West university</H1>
        <H2>Examinations Management Office</H2>
        <H1>Birendranagar, Surkhet</H1>
        <H3>Subject Triplicate</H3>
      </TopHeading>
      <BodyDiv>
        <LeftDiv>Central Campus of Humanities and Social Sciences</LeftDiv>
        <p >Second Semester</p>
      </BodyDiv>
      <StyledTable>
        <thead>
          <tr>
            <th style={{ width: "3%" }}>S.N.</th>
            <th style={{ width: "5%" }}>Year</th>
            <th style={{ width: "8%" }}>Program</th>
            <th style={{ width: "12%" }}>Name</th>
            <th style={{ width: "3%" }}>College</th>
            <th style={{ width: "6%" }}>Roll No</th>
            <th style={{ width: "4%" }}>Type</th>
            <th style={{ width: "12%" }}>Reg No</th>
            <th style={{ width: "5%" }}>Subj1</th>
            <th style={{ width: "5%" }}>Subj2</th>
            <th style={{ width: "5%" }}>Subj3</th>
            <th style={{ width: "5%" }}>Subj4</th>
            <th style={{ width: "5%" }}>Subj5</th>
            <th style={{ width: "5%" }}>Subj6</th>
            <th style={{ width: "5%" }}>Subj7</th>
            <th style={{ width: "5%" }}>Subj8</th>
            <th style={{ width: "5%" }}>Subj9</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => {
            return (
              <tr key={item.sn}>
                <td>{item.sn}</td>
                <td>{item.year}</td>
                <td>{item.program}</td>
                <td>{item.name}</td>
                <td>{item.college}</td>
                <td>{item.rollno}</td>
                <td>{item.type}</td>
                <td>{item.reg}</td>
                <td>{item.subj1}</td>
                <td>{item.subj2}</td>
                <td>{item.subj3}</td>
                <td>{item.subj4}</td>
                <td>{item.subj5}</td>
                <td>{item.subj6}</td>
                <td>{item.subj7}</td>
                <td>{item.subj8}</td>
                <td>{item.subj9}</td>
              </tr>
            );
          })}
        </tbody>
      </StyledTable>
    </Div>
  )
}

export default SubjectTriplicate