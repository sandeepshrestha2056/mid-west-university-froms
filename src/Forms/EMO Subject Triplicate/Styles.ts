import styled from "styled-components";

export const Div = styled.div`
  overflow-x :hidden;
  margin-top:2rem;
`;

export const TopHeading = styled.div`
      display         : flex;
      flex-direction  : column;
      justify-content : center;
      align-items     : center;
      line-height     : 0;
`;

export const H1 = styled.p`
font-size: .7rem;
`;

export const H2 = styled.p`
font-size: 1.2rem;
`;

export const H3 = styled.p`
font-size      : 1.3rem;
text-decoration: underline;
margin-top     : 1.5rem;
`;

export const BodyDiv = styled.div`
display        : flex;
justify-content: space-between;
margin         : 1rem 4rem 0 4rem;
`;
/* top | right | bottom | left */

export const LeftDiv = styled.p`
text-decoration: underline;
font-size      : 1.1rem;
font-weight    : bold;
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:99vw;
    margin-left:.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
`;