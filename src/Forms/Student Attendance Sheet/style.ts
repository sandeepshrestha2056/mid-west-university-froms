import styled from 'styled-components';

export const MainContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
`;

export const H1 = styled.p`
  text-transform: uppercase;
  text-align: center;
  font-size: 1.5rem;
  font-weight: bold;

`;

export const TopDiv = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 1.4rem;
  font-weight: bold;
  margin: 0 2rem;

`;

export const P = styled.p`
text-align:right;
margin-top: 2rem;
`;

export const StyledTable = styled.table`
      overflow-x: auto;
    border-spacing: 0;
    border:1px solid black;
    width:99vw;
    margin-left:2px ;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      color:black;
      margin: 0;
      padding: 0.5rem;
      font-size:1.2rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }

    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      font-size:1.2rem;

      :last-child {
        border-right: 0;

      } 
    }
  
`;