import React from 'react'
import { Container, H1, MainContainer, P, StyledTable, TopDiv } from './style'


const StudentAttendance = () => {
      const days: Array<number> = [];
      for (let i = 1; i < 32; i++) {
            days.push(i)

      }

      const data = [
            { rn: 1, name: "name1", pre: 23, att: 22, total: 26 },
            { rn: 1, name: "name1", pre: 23, att: 22, total: 26 },
            { rn: 1, name: "name1", pre: 23, att: 22, total: 26 },
            { rn: 1, name: "name1", pre: 23, att: 22, total: 26 },
            { rn: 1, name: "name1", pre: 23, att: 22, total: 26 },
            { rn: 1, name: "name1", pre: 23, att: 22, total: 26 },
      ]
      return (
            <MainContainer>
                  <Container>
                        <H1>Student's Attendance Sheet (20 )</H1>
                        <div>
                              <TopDiv>
                                    <p>Faculty:</p>
                                    <p>Level:</p>
                                    <p>Semester:</p>
                              </TopDiv>
                              <TopDiv>
                                    <p>Subject with Code No.:</p>
                                    <p>Year:</p>
                                    <p>Month:</p>
                              </TopDiv>
                        </div>

                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th rowSpan={2} style={{ width: "2%" }}>R.N.</th>
                                          <th rowSpan={2} style={{ width: "10%" }} >Name</th>
                                          <th colSpan={days.length} style={{ width: "79%" }} >Days</th>
                                          <th rowSpan={2} style={{ width: "3%" }}>Previous Att.</th>
                                          <th rowSpan={2} style={{ width: "3%" }}>Att. of this month</th>
                                          <th rowSpan={2} style={{ width: "1%" }}>Total Attendance</th>
                                    </tr>
                                    <tr>
                                          {
                                                days.map(i => <th key={i}>{i}</th>)
                                          }
                                    </tr>
                              </thead>
                              <tbody>
                                    {data.map(i => (
                                          <tr>
                                                <td>{i.rn}</td>
                                                <td>{i.name}</td>
                                                {days.map(i => <td>p</td>)}
                                                <td>{i.pre}</td>
                                                <td>{i.att}</td>
                                                <td>{i.total}</td>
                                          </tr>
                                    ))}

                              </tbody>
                        </StyledTable>
                        <P>Teacher Signature:_____________</P>
                  </Container>
            </MainContainer>
      )
}

export default StudentAttendance