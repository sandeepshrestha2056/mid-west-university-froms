import styled from 'styled-components';

export const Topbar = styled.div`
display: flex;
 flex-direction: column;
  align-items: center;
`;

export const P = styled.p`
  font-size:1.5rem;
  font-weight:bold;
  margin:0.3rem;
`;

export const Div = styled.div`
display: flex;
align-items: center;
justify-content: space-around;
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:99vw;
    margin-left:.4rem;
    font-size:1.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
    }
`;
