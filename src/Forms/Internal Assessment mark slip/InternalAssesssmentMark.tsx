import React from 'react'
import { Div, P, StyledTable, Topbar } from './style'

const InternalAssesssmentMark = () => {
      const data = [
            { sn: "1", name: "name", sym: "12", written: 23, home: 22, paper: 33, caseStudy: 11, att: 22, total: 33 },
            { sn: "2", name: "name", sym: "12", written: 23, home: 22, paper: 33, caseStudy: 11, att: 22, total: 33 },
            { sn: "3", name: "name", sym: "12", written: 23, home: 22, paper: 33, caseStudy: 11, att: 22, total: 33 },
            { sn: "4", name: "name", sym: "12", written: 23, home: 22, paper: 33, caseStudy: 11, att: 22, total: 33 },
            { sn: "5", name: "name", sym: "12", written: 23, home: 22, paper: 33, caseStudy: 11, att: 22, total: 33 },
      ]
      return (
            <div>
                  <div>
                        <Topbar>
                              <P>BABAI MULTIPLE CAMPUS</P>
                              <P>INTERNAL ASSESSMENT MARK SLIP</P>
                              <P>(....BATCH)</P>
                        </Topbar>
                        <Div >
                              <P>Level:</P>
                              <P>Date:</P>
                        </Div>
                        <Div >
                              <P>Semester:</P>
                              <P>Subject:</P>
                              <P>Code:</P>
                        </Div>
                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th>S.N</th>
                                          <th style={{ width: "20%" }}>Name of the Students</th>
                                          <th>Symbol No.</th>
                                          <th>Written Exam(Mid-Term F.M)</th>
                                          <th>Home Assignment F.M</th>
                                          <th>Paper/Class Presentation</th>
                                          <th>Field work case Study Project Work</th>
                                          <th>Attendance Participation</th>
                                          <th>Total 40</th>
                                    </tr>
                              </thead>
                              {data.map((item) => (
                                    <tr>
                                          <td>{item.sn}</td>
                                          <td>{item.name}</td>
                                          <td>{item.sym}</td>
                                          <td>{item.written}</td>
                                          <td>{item.home}</td>
                                          <td>{item.paper}</td>
                                          <td>{item.caseStudy}</td>
                                          <td>{item.att}</td>
                                          <td>{item.total}</td>
                                    </tr>
                              ))}


                              <tbody>

                              </tbody>
                        </StyledTable>
                  </div>
            </div>
      )
}

export default InternalAssesssmentMark