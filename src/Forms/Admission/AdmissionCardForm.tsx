import React from "react";
import {
  Container,
  F1,
  Headers,
  Heading1,
  Heading2,
  Heading3,
  Logo,
  Mid,
  PersonInfo,
  Right,
  Signatures,
  StyledTable,
} from "./StylesComponents";

const AdmissionCardForm = () => {
  const tableData = [
    {
      sn: 1,
      subCode: "IR521:",
      subjectName: "International Migration",
      theory: "",
      practical: "",
      remarks: "",
    },
    {
      sn: 2,
      subCode: "IR522:",
      subjectName: "Research Methodology Migration",
      theory: "",
      practical: "",
      remarks: "",
    },
    {
      sn: 3,
      subCode: "IR523:",
      subjectName: "Regional Study: South Asia",
      theory: "",
      practical: "",
      remarks: "",
    },
    {
      sn: 4,
      subCode: "IR524:",
      subjectName: "Security of Small State",
      theory: "",
      practical: "",
      remarks: "",
    },
    {
      sn: 5,
      subCode: "IR525:",
      subjectName: "Global COnflict and Globalization",
      theory: "",
      practical: "",
      remarks: "",
    },
  ];
  return (
    <Container>
      <Headers>
        <Logo>
          <img
            src="https://image.shutterstock.com/image-vector/earth-globe-icon-trendy-logo-260nw-1257541729.jpg"
            alt="logo"
          />
        </Logo>
        <Mid>
          <Heading1>Mid-West University</Heading1>
          <Heading2>Examination Management Office </Heading2>
          <Heading2>Birendranagar, Surkhet</Heading2>
          <Heading3>Admission Card</Heading3>
        </Mid>
        <Right>
          <img
            src="https://cdn.pixabay.com/photo/2015/03/04/22/35/head-659652_1280.png"
            alt="personImg"
          />
        </Right>
      </Headers>
      <PersonInfo>
        <p className="roll-no">Exam Roll No: 210000000</p>
        <F1>
          <p className="fullName">Full Name: KHIM BHAHADUR SHAHI</p>
          <p className="regd-no">Regd.no: 2020-69-3-10030032</p>
        </F1>
        <p className="campus">
          Campus: Central Campus of Humanities and Social Science
        </p>
        <F1>
          <p className="level">Level: Master</p>
          <p className="examType">Exam: Regular</p>
          <p className="semester">Semester: Second Semester</p>
        </F1>
        <p className="specialization-subject">
          Specialization-Subject: Master of Arts
        </p>
        <F1>
          <p className="exam-center">
            Exam Center: Central Campus of Humanities and Social Science
          </p>
          <p className="exam-year">Exam Year: 2021</p>
        </F1>
      </PersonInfo>
      <StyledTable>
        <thead>
          <tr>
            <th style={{ width: "5%" }}>S.N.</th>
            <th style={{ width: "10%" }}>SubCode</th>
            <th style={{ width: "40%" }}>Subject Name</th>
            <th style={{ width: "15%" }}>Theory</th>
            <th style={{ width: "15%" }}>Practical</th>
            <th style={{ width: "15%" }}>Remarks</th>
          </tr>
        </thead>
        <tbody>
          {tableData.map((item) => {
            return (
              <tr key={item.sn}>
                <td>{item.sn}</td>
                <td>{item.subCode}</td>
                <td>{item.subjectName}</td>
                <td>{item.theory}</td>
                <td>{item.practical}</td>
                <td>{item.remarks}</td>
              </tr>
            );
          })}
        </tbody>
      </StyledTable>
      <Signatures>
        <p >Signature of Applicant</p>
        <p>Signature of Superintendent</p>
        <p>Signature of Examinations Management Chief</p>
      </Signatures>
    </Container>
  );
};

export default AdmissionCardForm;
