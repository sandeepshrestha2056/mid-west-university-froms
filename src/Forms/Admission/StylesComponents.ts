import styled from "styled-components";

export const StyledTable = styled.table`
  padding-top: 1rem;
    border-spacing: 0;
    border: 1px solid black;
    width: 82vw;
    margin-left: 10rem;
    margin-top: 1rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #ccc;
  overflow-x: hidden;
`;
export const Headers = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const Logo = styled.div`
  img {
    max-width: 5vmax;
  }
`;

export const Mid = styled.div`
  padding: 2vmax;
`;

export const Heading1 = styled.p`
  font-size: 2rem;
  margin-bottom: 0.4rem;
  text-align: center;
`;

export const Heading2 = styled.p`
  font-size: 1.5rem;
  text-align: center;
  line-height: 0;
`;

export const Heading3 = styled.p`
  padding-top: 1rem;
  text-align: center;
  font-size: 1.5rem;
  text-decoration: underline;
`;

export const Right = styled.div`
  img {
    max-width: 10vmax;
  }
`;

export const PersonInfo = styled.div`
  display: grid;
  width: 82vw;
  justify-content: space-between;
  margin-left: 10rem;
  margin-right: -10rem;
`;

export const F1 = styled.div`
  display: flex;
  width: 82vw;
  justify-content: space-between;
`;

export const Signatures = styled.div`
  width: 82vw;
  margin-top: 2.4rem;
  margin-bottom: 1rem;
  margin-left: 10rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
