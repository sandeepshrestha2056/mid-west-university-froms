import React from 'react'
import { Heading, StyledTable } from '../Darta Book/styled'

const ChalaniBook = () => {
      const data = [
            { chalanino: 1, Date: "2056/12/3", patrasankhya: 2, subject: "sub", pathyayeko: "sadhyan", last: "last" },
            { chalanino: 2, Date: "2056/12/4", patrasankhya: 4, subject: "sub2", pathyayeko: "sadhyan1", last: "las4t" },
            { chalanino: 2, Date: "2056/12/5", patrasankhya: 1, subject: "sub4", pathyayeko: "sadhyan2", last: "last44" },
      ]
      return (
            <div>
                  <Heading>Chalani Kitab</Heading>
                  <StyledTable>
                        <thead>
                              <tr>
                                    <th>chanlani No</th>
                                    <th>Date</th>
                                    <th>Patra Sankhya</th>
                                    <th>Subject</th>
                                    <th>Pathayeko Sadhan</th>
                                    <th style={{ borderRight: 0 }}>Last</th>
                              </tr>
                        </thead>

                        {data.map((item) => (
                              <tr key={item.Date}>
                                    <td>{item.chalanino}</td>
                                    <td>{item.Date}</td>
                                    <td>{item.patrasankhya}</td>
                                    <td>{item.subject}</td>
                                    <td>{item.pathyayeko}</td>
                                    <td>{item.last}</td>
                              </tr>
                        ))}

                        <tbody>

                        </tbody>
                  </StyledTable>

            </div>
      )
}

export default ChalaniBook