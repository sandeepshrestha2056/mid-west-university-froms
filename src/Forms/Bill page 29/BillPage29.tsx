import React from 'react'
import { Container, Header, MainContainer, StyledTable } from './style'

const BillPage29 = () => {
      const data = [
            { date: "2014/2/3", signature: "sign" },
            { date: "2015/2/5", signature: "signature" },
            { date: "2015/2/6", signature: "sign" },
            { date: "2016/2/7", signature: "sign" },
            { date: "2017/2/3", signature: "sign" },
            { date: "2016/2/7", signature: "sign" },
            { date: "2017/2/3", signature: "sign" },
      ]
      return (
            <MainContainer>
                  <Container>
                        <Header>
                              <p>BOOK REGD. NO.:</p>
                              <p>AUTHOR</p>
                              <p>TITLE</p>
                        </Header>
                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th>DATE DUE</th>
                                          <th>BORROWER'S NAME</th>
                                    </tr>
                              </thead>
                              <tbody>
                                    {data.map((item, i) => (
                                          <tr key={i}>
                                                <td>{item.date}</td>
                                                <td>{item.signature}</td>
                                          </tr>
                                    ))}
                              </tbody>
                        </StyledTable>
                  </Container>
            </MainContainer>
      )
}

export default BillPage29