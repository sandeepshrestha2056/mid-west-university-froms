import styled from 'styled-components';

export const MainContainer = styled.div`
display:flex;
justify-content:center;
align-items:center;
`;

export const Container = styled.div`
  width: 50vw;
  border: 1px solid black;
  padding: 1.3rem;
  background-color: rgb(202, 202, 24)
`;

export const Header = styled.div`
p{
      font-size: 1.3rem;
      border-bottom: 1px solid black;
      font-weight:bold;
}  
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:49vw;
    margin-left:.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
  }
`;