import React from 'react'
import { Container, HeaderInfo, Heading, StyledTable } from './style'

const ShortTermObjList = () => {
      const data = [
            { date: "2056/12/12", dakhilano: "22", pariman1: "23", dar1: "2", racam1: "1200", pariman2: "23", dar2: "2", racam2: "1200", pariman3: "23", dar3: "2", racam3: "1200", last: "lsrt" },
            { date: "2056/12/12", dakhilano: "22", pariman1: "23", dar1: "2", racam1: "1200", pariman2: "23", dar2: "2", racam2: "1200", pariman3: "23", dar3: "2", racam3: "1200", last: "lsrt" },
            { date: "2056/12/12", dakhilano: "22", pariman1: "23", dar1: "2", racam1: "1200", pariman2: "23", dar2: "2", racam2: "1200", pariman3: "23", dar3: "2", racam3: "1200", last: "lsrt" },
            { date: "2056/12/12", dakhilano: "22", pariman1: "23", dar1: "2", racam1: "1200", pariman2: "23", dar2: "2", racam2: "1200", pariman3: "23", dar3: "2", racam3: "1200", last: "lsrt" },
      ]
      return (
            <Container>
                  <div>
                        <Heading>heading</Heading>
                        <HeaderInfo>
                              <div >
                                    <p>left1</p>
                                    <p>left2</p>
                                    <p>left3</p>
                              </div>
                              <div>
                                    <p>right1</p>
                                    <p>right2</p>
                                    <p>right3</p>
                              </div>
                        </HeaderInfo>

                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th rowSpan={2}>date</th>
                                          <th rowSpan={2}>Dakhlila No</th>
                                          <th colSpan={3} >store dakhila(aamdani)</th>
                                          <th colSpan={3}>nikasa(kharcha)</th>
                                          <th colSpan={3}>baki</th>
                                          <th rowSpan={2}>last</th>
                                    </tr>
                                    <tr>
                                          <th>pariman</th>
                                          <th>dar</th>
                                          <th>racam</th>
                                          <th>pariman</th>
                                          <th>dar</th>
                                          <th>racam</th>
                                          <th>pariman</th>
                                          <th>dar</th>
                                          <th>racam</th>
                                    </tr>
                                    <tr>
                                          <th>1</th>
                                          <th>2</th>
                                          <th>3</th>
                                          <th>4</th>
                                          <th>5</th>
                                          <th>6</th>
                                          <th>7</th>
                                          <th>8</th>
                                          <th>9</th>
                                          <th>10</th>
                                          <th>11</th>
                                          <th>12</th>


                                    </tr>
                              </thead>
                              <tbody>
                                    {data.map((item, i) => (
                                          <tr>
                                                <td>{item.date}</td>
                                                <td>{item.dakhilano}</td>
                                                <td>{item.pariman1}</td>
                                                <td>{item.dar1}</td>
                                                <td>{item.racam1}</td>
                                                <td>{item.pariman2}</td>
                                                <td>{item.dar2}</td>
                                                <td>{item.racam2}</td>
                                                <td>{item.pariman3}</td>
                                                <td>{item.dar3}</td>
                                                <td>{item.racam3}</td>
                                                <td>{item.last}</td>
                                          </tr>

                                    ))}
                              </tbody>
                        </StyledTable>
                  </div>
            </Container>
      )
}

export default ShortTermObjList