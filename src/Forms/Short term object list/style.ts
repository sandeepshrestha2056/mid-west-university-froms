import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Heading = styled.p`
text-align: center;
font-size: 2rem;
`;

export const HeaderInfo = styled.div`
display: flex;
justify-content: space-between;
margin: 1rem;
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:70vw;
    margin-left:.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
    }
`;
