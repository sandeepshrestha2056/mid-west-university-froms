import styled from 'styled-components';

export const MainContainer = styled.div`
display        : flex;
justify-content: center;
margin         : 1rem 0;
`;

export const Container = styled.div`
width       : 40vw;
border      : 1px solid black;
border-color: red;
border-top  : none
`;

export const Heading = styled.div`
  background-color:black;
  color:white;
  padding:1rem 0;
`;

export const H1 = styled.p`
  font-size:1.5rem;
  text-align: center;
`;
export const H2 = styled.p`
  font-size:2rem;
  text-align: center;
  font-weight: bold;
`;

export const H3 = styled.p`
font-size:2rem;
text-align: center;
font-weight: bold;
background-color:red;
padding:.5rem 0;
margin:0;
color: white;
`;
export const H4 = styled.p`
font-size:2rem;
text-align: center;
padding:.5rem 0;
margin:0;
`;

export const AcronymsDIv = styled.div`
margin-left:2rem;
p{
  color:red;
  font-size:1.5rem;
  font-weight:bold;
  text-decoration:underline;
}
li{
  font-size:1.2rem;
  padding:.5rem 0;
}
  
`;
export const GradingDiv = styled.div`
margin-left:2rem;
p{
  color:red;
  font-size:1.5rem;
  font-weight:bold;
  text-decoration:underline;
}
span{
  font-size:1.2rem;
}
`;
export const Img = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
`;


export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:39vw;
    margin-left:.4rem;
    font-size:1.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
    }
`;
