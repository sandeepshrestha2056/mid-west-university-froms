import React from 'react'
import { AcronymsDIv, Container, GradingDiv, H1, H2, H3, H4, Heading, Img, MainContainer, StyledTable } from './style'
import image from "../../images/ss.png";

const DeterminationLetterGrading = () => {
      const acronyms = [
            { name: "Abs stands for absent." },
            { name: "SGPA stands for Semester wise Grading Point Average." },
            { name: "GP stands for Grading Point." },
            { name: "Cr.Hr. stands for Credit Hours." },
            { name: "Ex. stands for Expelled." },
            { name: "Wh. stands for Withheld." },
      ]

      const data = [
            { grade: "A", range: "85-100", gradeCategory: "Outstanding", gradePoint: "4.00" },
            { grade: "A-", range: "80-84", gradeCategory: "Distinction", gradePoint: "3.67" },
            { grade: "B", range: "75-79", gradeCategory: "Excellent", gradePoint: "3.33" },
            { grade: "B-", range: "70-74", gradeCategory: "Very Good", gradePoint: "3.00" },
            { grade: "C", range: "65-69", gradeCategory: "Good", gradePoint: "2.50" },
            { grade: "C-", range: "60-64", gradeCategory: "Average", gradePoint: "2.00" },
            { grade: "D", range: "55-59", gradeCategory: "Satisfactory", gradePoint: "1.50" },
            { grade: "D-", range: "50-54", gradeCategory: "Fair", gradePoint: "1.00" },
            { grade: "F", range: "<50", gradeCategory: "Fail", gradePoint: "0.00" },
      ]
      return (
            <MainContainer>
                  <Container>
                        <Heading>
                              <H1>Mid-West University</H1>
                              <H2>Babai Multiple Campus</H2>
                              <H1>Gulariya, Bardiya</H1>
                        </Heading>
                        <H3>Bachelor & Master Degree Programme</H3>
                        <H4>Determination of Letter Grading:</H4>
                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th>Grade</th>
                                          <th>Range %</th>
                                          <th>Grade Category</th>
                                          <th>Grade Point</th>
                                    </tr>
                              </thead>
                              {data.map((item, i) => (
                                    <tr key={i}>
                                          <td>{item.grade}</td>
                                          <td>{item.range}</td>
                                          <td>{item.gradeCategory}</td>
                                          <td>{item.gradePoint}</td>
                                    </tr>
                              ))}

                              <tbody>

                              </tbody>
                        </StyledTable>

                        <AcronymsDIv>
                              <p>Acronyms :-</p>
                              <ol>
                                    {
                                          acronyms.map((item) => (
                                                <li>{item.name}</li>
                                          ))
                                    }
                              </ol>
                        </AcronymsDIv>
                        <GradingDiv>
                              <p>Grading Point Average (GPA) Evaluation Method :-</p>
                              <Img>
                                    <img src={image} alt="img" />
                              </Img>

                        </GradingDiv>
                        <GradingDiv>
                              <p>Credits :-</p>
                              <span>1 credit is equivalent to 16 Teaching Hours in the Teaching Part, Internship and<br /> Thesis Writing.</span>
                        </GradingDiv>
                  </Container>
            </MainContainer>
      )
}

export default DeterminationLetterGrading