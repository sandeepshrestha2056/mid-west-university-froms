import React from 'react'
import { BottomDiv, H1, StyledTable, Topbar } from './style'

const ResultAnalysis = () => {
      const data = [
            { name: "Nep.401-Com.Nepali" },
            { name: "Eng.411-Com.Nepali" },
            { name: "Ed.412-Philosophy & Found.Of Edu" },
            { name: "Hp.416-Found.Of Health Edu." },
            { name: "Hp.417TH-Found.Of Phy.Edu." },
            { name: "Hp.417TH-Found.Of Phy.Edu." },
            { name: "Hp.418-Found.Of Pop.Edu." },
      ]
      return (
            <div>
                  <div>
                        <Topbar>
                              <p>Babai Multiple Campus</p>
                              <p>Gulariya, Bardiya</p>
                              <p>Result Analysis</p>
                              <p>Academic Year-2073</p>
                        </Topbar>
                        <H1>
                              <p>Health and Physical Education</p>
                              <p>Programme:</p>
                        </H1>
                        <H1>
                              <p>Education</p>
                              <p>Year:First</p>
                        </H1>
                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th rowSpan={2}>S.N</th>
                                          <th rowSpan={2}>Sub. Code</th>
                                          <th rowSpan={2}>Total Appeared</th>
                                          <th rowSpan={2}>Total Passed</th>
                                          <th rowSpan={2}>Total Failed</th>
                                          <th>Boys</th>
                                          <th>Girls</th>
                                          <th>EDJ</th>
                                          <th>Madhasi</th>
                                          <th>Dalit</th>
                                          <th rowSpan={2}>Orders</th>
                                          <th style={{ borderRight: 0 }} rowSpan={2}>Last</th>
                                    </tr>
                                    <tr>
                                          <th>TP</th>
                                          <th>TP</th>
                                          <th>TP</th>
                                          <th>TP</th>
                                          <th>TP</th>
                                    </tr>
                              </thead>


                              <tbody>

                              </tbody>
                        </StyledTable>
                        <BottomDiv>
                              {data.map((item) => (
                                    <ul>
                                          <li>{item.name}</li>
                                    </ul>
                              ))}
                        </BottomDiv>
                  </div>
            </div>
      )
}

export default ResultAnalysis