import styled from 'styled-components';

export const Topbar = styled.div`
  display:flex;
  justify-content:center;
  flex-direction:column;
  align-items:center;
  p{
    font-size:1.5rem;
    margin:0.4rem;
    font-weight:bold;
  }
`;

export const H1 = styled.div`
display:flex;
justify-content:space-around;
align-items:center;
p{
  font-size:1.1rem;
  margin:0.4rem;
  font-weight:bold;
}
`;

export const BottomDiv = styled.div`
  margin-left:1.5rem;
  li{
    font-size:1.1rem;
  }
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:99vw;
    margin-left:.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
    }
`;
