import styled from 'styled-components';

export const MainContainer = styled.div`
display        : flex;
justify-content: center;
flex-direction : column;
align-items    : center;

`;
export const Container = styled.div`
  width: 40vw;
  border: 1px solid black;
`;

export const Topbar = styled.div`
height     : 40vh;
border     : 1px solid black;
color            : darkblue;
h1{
      font-size        : 2vmax;
      width            : 40vw;
      text-align       : center;
      position         : relative;
      left             : 50%;
      top              : calc(30% - 100px);
      -webkit-transform: translate(-50%, calc(-50% - 64px));
      -ms-transform    : translate(-50%, calc(-50% - 64px));
      transform        : translate(-50%, calc(-50% - 64px));

      span{
            position: fixed;
            width:  5vmax;
      }
}
p:last-child{
      text-align: center;
      font-size : 1.2rem;
}
`;

export const H2 = styled.div`
background-color: darkblue;
p{
      color      : white;
      text-align : center;
      font-size  : 1.5rem;
      font-weight: bold;
      margin:0;
}
`;

export const TopHeader = styled.div`
      display        : flex;
      justify-content: space-between;
      margin         : 1.5rem;

img{
      max-width: 5vmax;
}
  
`;

export const AddressDiv = styled.div`
margin   : 2rem 16rem;
font-size: 1.2rem;
padding  : .5rem;
width    : 10vmax;

border:none;
p:first-child{
      text-align : center;
      font-size  : 1.3rem;
      font-weight: bold;
}
p:last-child{
      text-align: center;
      font-size : 0.9rem;
}
`;

export const BottomBar = styled.div`
  display: flex;
  justify-content: space-around;
  padding-top:2rem;
  background-color:black;
  color: white;
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border:1px solid rgb(75, 124, 216);
    width:40vw;
    padding-right: -3px;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      background-color:rgb(75, 124, 216);
      color:white;
      margin: 0;
      padding: 0.5rem;
      font-size:1.4rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      font-size:1.2rem;

      :last-child {
        border-right: 0;

      } 
    }
`;
