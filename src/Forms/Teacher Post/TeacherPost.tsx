import React, { useRef } from 'react'
import { AddressDiv, BottomBar, Container, H2, MainContainer, StyledTable, Topbar, TopHeader } from './style';
import ReactToPrint from 'react-to-print';

const TeacherPost = () => {
      const currentRef = useRef(null);
      const text = "BABAI MULTIPLE CAMPUS";
      const radius = 25;
      const arc = 95;
      const characters = text.split('');
      const degree = arc / characters.length;

      const data = [
            { sn: 1, name: "name", post: 'post', subject: "subject" },
            { sn: 2, name: "name", post: 'post', subject: "subject" },
            { sn: 3, name: "name", post: 'post', subject: "subject" },
            { sn: 4, name: "name", post: 'post', subject: "subject" },

      ]
      return (
            <MainContainer >
                  <Container ref={currentRef}>
                        <Topbar>
                              <TopHeader>
                                    <img src="https://image.shutterstock.com/image-vector/earth-globe-icon-trendy-logo-260nw-1257541729.jpg" alt="logo" />
                                    <p>084-4202020</p>
                              </TopHeader>
                              <h1>
                                    {characters.map((char, i) => (
                                          <span
                                                key={`heading-span-${i}`}
                                                style={{
                                                      height: `${radius}rem`,
                                                      transform: `rotate(${degree * i - arc / 2}deg)`,
                                                      transformOrigin: `0 ${radius}rem 0`,
                                                }}>
                                                {char}
                                          </span>
                                    ))}
                              </h1>
                              <AddressDiv>
                                    <p>Gulariya, Bardiya</p>
                                    <p>Est in 2004</p>
                              </AddressDiv>
                              <p>Constituent Campus Of Mid-West University, Surkhet</p>
                        </Topbar>
                        <H2>
                              <p>Education, Population,Education, Health Education, Curriculum, <br /> Planning & Management Instruction Committee-2078 </p>
                        </H2>
                        <StyledTable>
                              <thead>
                                    <tr>
                                          <th>SN</th>
                                          <th>Name of Members</th>
                                          <th>Post</th>
                                          <th>Subject</th>
                                    </tr>
                              </thead>
                              <tbody>
                                    {data.map((item, i) => (
                                          <tr>
                                                <td>{item.sn}</td>
                                                <td>{item.name}</td>
                                                <td>{item.post}</td>
                                                <td>{item.subject}</td>
                                          </tr>

                                    ))}
                              </tbody>
                        </StyledTable>
                        <BottomBar>
                              <div>
                                    <p>Name</p>
                                    <p>post</p>
                                    <p>Institution Name</p>
                              </div>
                              <div>
                                    <p>Name</p>
                                    <p>post</p>
                                    <p>Institution Name</p>
                              </div>
                        </BottomBar>
                  </Container>
                  <ReactToPrint trigger={() => <button>Print</button>} content={() => currentRef.current} />

            </MainContainer>

      )
}

export default TeacherPost