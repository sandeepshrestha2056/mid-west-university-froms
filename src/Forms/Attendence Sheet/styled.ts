import styled from 'styled-components';

export const TopRight = styled.p`
  text-align: right;
  margin-right:1rem;
`;

export const TopBar = styled.div`
  display              : grid;
  grid-template-columns: 5fr 2fr 1fr 1fr 5fr;
  margin: 0 .6rem;
  justify-content      : center;
  align-items          : center;
  p:last-child{
      text-align: right;
  }
`;

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:99vw;
    margin-left:.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
    }
`;

export const PointsDiv = styled.div`
display: flex;
 margin-left: 1rem; 
`;

export const Ul = styled.ul`
  list-style:none;
`;

export const BottomDiv = styled.div`
display: flex;
 justify-content: space-between;
 margin: 1rem 1rem;
 width:70vw;
 p:first-child{
   text-decoration:underline;
   font-size:1.2rem;
 }
`;