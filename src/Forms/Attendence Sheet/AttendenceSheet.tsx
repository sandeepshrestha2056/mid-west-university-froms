import React from 'react'
import { BottomDiv, PointsDiv, StyledTable, TopBar, TopRight, Ul } from './styled'

const AttendenceSheet = () => {
      const data = [
            { bibran: "sal samma ko", jama1: 12, kharcha1: 2, baki1: 4, jama2: 12, kharcha2: 2, baki2: 4, jama3: 12, kharcha3: 2, baki3: 4, jama4: 12, kharcha4: 2, baki4: 4, jama5: 12, kharcha5: 2, baki5: 4, jama6: 12, kharcha6: 2, baki6: 4, jama7: 12, kharcha7: 2, baki7: 4, last: "last" },
            { bibran: "sal ma", jama1: 12, kharcha1: 2, baki1: 4, jama2: 12, kharcha2: 2, baki2: 4, jama3: 12, kharcha3: 2, baki3: 4, jama4: 12, kharcha4: 2, baki4: 4, jama5: 12, kharcha5: 2, baki5: 4, jama6: 12, kharcha6: 2, baki6: 4, jama7: 12, kharcha7: 2, baki7: 4, last: "last" },
            { bibran: "s", jama1: 12, kharcha1: 2, baki1: 4, jama2: 12, kharcha2: 2, baki2: 4, jama3: 12, kharcha3: 2, baki3: 4, jama4: 12, kharcha4: 2, baki4: 4, jama5: 12, kharcha5: 2, baki5: 4, jama6: 12, kharcha6: 2, baki6: 4, jama7: 12, kharcha7: 2, baki7: 4, last: "last" },
      ]

      const points = [
            { li: "points 1" },
            { li: "points 2" },
            { li: "points 3" },
            { li: "points 4" },
      ]

      return (
            <div>
                  <TopRight>Top right</TopRight>
                  <TopBar>
                        <p>Name, Thar:-  </p>
                        <p>Darja:-</p>
                        <p>20</p>
                        <p>sal</p>
                        <p>samma ko hajhir ra aaushdi ko hajhir ra aaushdi u</p>
                  </TopBar>
                  <StyledTable>
                        <thead>
                              <tr>
                                    <th rowSpan={2}>Bibaran</th>
                                    <th colSpan={3}>Causal Bida</th>
                                    <th colSpan={3}>Ghar Bida</th>
                                    <th colSpan={3}>Birami Bida</th>
                                    <th colSpan={3}>Parba Bida</th>
                                    <th colSpan={3}>Addhyan Bida</th>
                                    <th colSpan={3}>Ashadharan Bida</th>
                                    <th colSpan={3}>Aausadhi Upachar</th>
                                    <th rowSpan={2}>last</th>
                              </tr>
                              <tr>
                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>

                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>

                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>

                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>

                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>

                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>

                                    <th>Jamma</th>
                                    <th>Kharcha</th>
                                    <th>Baki</th>
                              </tr>
                        </thead>
                        <tbody>
                              {
                                    data.map((item, i) => (
                                          <tr key={i}>
                                                <th>{item.bibran}</th>
                                                <td>{item.baki1}</td>
                                                <td>{item.kharcha1}</td>
                                                <td>{item.baki1}</td>
                                                <td>{item.baki2}</td>
                                                <td>{item.kharcha2}</td>
                                                <td>{item.baki2}</td>
                                                <td>{item.baki3}</td>
                                                <td>{item.kharcha3}</td>
                                                <td>{item.baki3}</td>
                                                <td>{item.baki4}</td>
                                                <td>{item.kharcha4}</td>
                                                <td>{item.baki4}</td>
                                                <td>{item.baki5}</td>
                                                <td>{item.kharcha5}</td>
                                                <td>{item.baki5}</td>
                                                <td>{item.baki6}</td>
                                                <td>{item.kharcha6}</td>
                                                <td>{item.baki6}</td>
                                                <td>{item.baki7}</td>
                                                <td>{item.kharcha7}</td>
                                                <td>{item.baki7}</td>
                                                <td>{item.last}</td>
                                          </tr>
                                    ))
                              }
                        </tbody>
                  </StyledTable>

                  <PointsDiv>
                        <p>Points:-</p>
                        <div>
                              {points.map((item, i) => (
                                    <Ul key={i}>
                                          <li>{i + 1}) {item.li}</li>
                                    </Ul>
                              ))}
                        </div>
                  </PointsDiv>
                  <BottomDiv >
                        <div>
                              <p>abhilekh </p>
                              <p>name</p>
                              <p>paad</p>
                              <p>signature</p>
                              <p>Date</p>
                        </div>
                        <div>
                              <p >1) rujha garne</p>
                              <p>name</p>
                              <p>paad</p>
                              <p>signature</p>
                              <p>Date</p>
                        </div>
                        <div>
                              <p>2) ruj garne</p>
                              <p>name</p>
                              <p>paad</p>
                              <p>signature</p>
                              <p>Date</p>
                        </div>
                  </BottomDiv>
            </div>
      )
}

export default AttendenceSheet