import styled from 'styled-components';

export const StyledTable = styled.table`
    border-spacing: 0;
    border: 1px solid black;
    width:99vw;
    margin-left:.4rem;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 1 px solid black;
      }
    }
    td{
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;

      } 
    }
`;

export const Heading = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`;

export const Bottom = styled.div`
  display:flex;
  justify-content:space-around;
`;