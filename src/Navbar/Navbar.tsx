import React from 'react';
import { Link } from "react-router-dom";

const Navbar = () => {
      return (
            <div>
                  <ol>
                        <li><Link to="/">AdmissionCardForm</Link></li>
                        <li><Link to="/subjecttriplicate">SubjectTriplicate</Link></li>
                        <li><Link to="/registration">Registration</Link></li>
                        <li><Link to="/dartaBook">DartaBook</Link></li>
                        <li><Link to="/attendenceSheet">AttendenceSheet</Link></li>
                        <li><Link to="/salarydetails">SalaryDetails</Link></li>
                        <li><Link to="/shorttermbbjList">ShortTermObjList</Link></li>
                        <li><Link to="/determinationlettergrading">DeterminationLetterGrading</Link></li>
                        <li><Link to="/pichart">pichart</Link></li>
                        <li><Link to="/internalassesssmentmark">InternalAssesssmentMark</Link></li>
                        <li><Link to="/resultanalysis">ResultAnalysis</Link></li>
                        <li><Link to="/libraryCard">libraryCard</Link></li>
                        <li><Link to="/billpage29">BillPage29</Link></li>
                        <li><Link to="/barchart">Bar chart</Link></li>
                        <li><Link to="/teacherpost"> Teacher Post</Link></li>
                        <li><Link to="/pictureno33"> Picture 33</Link></li>
                        <li><Link to="/routine"> Routine</Link></li>
                        <li><Link to="/studentattendance"> Student Attendance Sheet</Link></li>
                        <li><Link to="/routineoffirstndthird"> Routine of First & Third semester</Link></li>
                  </ol>
            </div>
      )
}

export default Navbar